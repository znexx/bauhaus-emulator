#include"create_window.h"

int create_window(Window* window, uint16_t w, uint16_t h, const char* title){
	window->width = w;
	window->height = h;

	window->win = SDL_CreateWindow(title,100,100,w,h,SDL_WINDOW_SHOWN);
	if(!window->win){
		create_window_error("SDL_CreateWindow",SDL_GetError());
		return 1;
	}

	window->ren = SDL_CreateRenderer(window->win,-1,SDL_RENDERER_SOFTWARE);
	if(!window->ren){
		create_window_error("SDL_CreateRenderer",SDL_GetError());
		return 2;
	}
	
	window->surf = SDL_GetWindowSurface(window->win);
	if(!window->surf){
		create_window_error("SDL_GetWindowSurface",SDL_GetError());
		return 3;
	}
	return 0;
}

int destroy_window(Window* window){
	SDL_DestroyRenderer(window->ren);
	SDL_DestroyWindow(window->win);
	return 0;
}
void create_window_error(char* type, const char* error){
	fprintf(stderr, "%s error: %s\n",type,error);
}
