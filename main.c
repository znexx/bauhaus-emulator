#include<main.h>
static uint8_t init(int, char**);
static uint8_t init_SDL(void);
static uint8_t term(void);
static uint8_t term_SDL(void);
static uint8_t loop(void);
extern uint8_t tick(void);

static SDL_Thread*	cpu_thread;
static SDL_Event*	event;
static Window*		screen;
CPU*		cpu;

static int	return_value;

int main(int argc, char** argv){
	return_value = init(argc, argv);
	if(return_value){
		term();
		return return_value;
	}

	loop();

	term();

	return 0;
}

uint8_t loop(void){
	while(cpu->running){
		while(SDL_PollEvent(event)){
			switch(event->type){
				case SDL_QUIT:
					cpu->running = false;
					printf("Quit was pressed\n");
					break;
				case SDL_KEYDOWN:
					
					break;
				case SDL_KEYUP:
					break;
			}
		}
		tick();
		//interrupt();
	}
	return 0;
}

uint8_t init_SDL(void){
	return_value = SDL_Init(SDL_INIT_EVERYTHING);
	if(return_value){
		SDL_Log("Error initializing SDL: %s\n",SDL_GetError());
		return return_value;
	}

	event = malloc(sizeof(SDL_Event));
	if(!event){
		SDL_Log("Error allocating event object\n");
		return 1;
	}

	screen = malloc(sizeof(Window));
	if(!screen){
		SDL_Log("Error allocating window\n");
		return 1;
	}

	return_value = create_window(screen, 320, 200, "Emulator window");
	if(return_value){
		SDL_Log("Error creating window\n");
		return return_value;
	}
	return 0;
}
uint8_t term_SDL(void){
	SDL_WaitThread(cpu_thread,&return_value);
	destroy_window(screen);
	free(screen);
	free(event);
	SDL_Quit();
	return 0;
}
uint8_t init(int argc, char** argv){
	int16_t c;
	uint8_t n = 0;

	for(n=0; n<argc; n++){
		printf("arg %d: %s\n", n, argv[n]);
	}

	return_value = init_SDL();
	if(return_value){
		return return_value;
	}

	cpu = calloc(2,sizeof(CPU));
	while((c = getchar()) != EOF){
		printf("%02x: %02x\n",n,c);
		cpu->ram[n++] = c;
	}
	cpu->vram = screen->surf->pixels;
	cpu->running = true;

	return 0;
}
uint8_t term(void){
	term_SDL();
	return 0;
}
