#include "cpu.h"
static uint8_t	read_mem(uint32_t);
static uint16_t	read_mem16(uint32_t);
static uint8_t	write_mem(uint32_t, uint8_t);
static uint16_t	write_mem16(uint32_t, uint16_t);

static uint16_t	read_port(uint16_t);
static uint16_t	write_port(uint16_t, uint16_t);

static uint16_t read_spec(uint8_t*);
static uint16_t read_operand(uint16_t*, uint8_t);
static uint16_t write_operand(uint8_t, uint16_t);

uint8_t	tick();

extern CPU*	cpu;

#define reg(x) cpu->regs[x]
#define DEBUG

#ifdef DEBUG
	#define printf_debug(...) printf(__VA_ARGS__);
#else
	#define printf_debug(...)
#endif

uint8_t tick(){
	uint16_t	in1 = 0,
			in2 = 0,
			out,
			opcode,
			instruction_code,
			spec_codes;
	uint8_t		spec;
	write_mem(0,read_mem(0));
	opcode = read_mem(reg(0));
	instruction_code = opcode >> 9;
	spec_codes  = opcode & 0x01ff;
	printf_debug("%04x op: %02x\n", reg(0), read_mem(reg(0)));
	reg(0) += 1;
	switch(opcode){
		#include "instructions/special.c"
		#include "instructions/or.c"
		#include "instructions/and.c"
		#include "instructions/xor.c"
		#include "instructions/add.c"
		#include "instructions/sub.c"
		#include "instructions/shl.c"
		#include "instructions/shr.c"
		#include "instructions/je.c"
		#include "instructions/jne.c"
		#include "instructions/jg.c"
		#include "instructions/jge.c"
		#include "instructions/in.c"
		#include "instructions/out.c"
	}
	return 0;
}

uint16_t read_spec(uint8_t* spec){
	*spec = read_mem(reg(0));
	reg(0) += 1;
	return 0;
}

uint16_t read_port(uint16_t port){
	printf_debug("    |-port: %04x\n", port);
	switch(port){
		case 0x01:
			return getchar();
		default:
			return 0;
	}
}

uint16_t write_port(uint16_t port, uint16_t data){
	printf_debug("    |-port: %04x\n", port);
	switch(port){
		case 0x01:
			putchar(data);
		default:
			data = data;
	}
	return 0;
}

uint16_t write_operand(uint8_t spec, uint16_t value){
	uint16_t	address;
	printf_debug("    |-spec: %02x\n",spec);
	if(spec & 0x80){
		value &= 0xff;
	}
	switch(spec & 0x07){
		case 0x00: // reg
			reg(read_mem(reg(0))) = value;
			reg(0) += 1;
			break;
		case 0x01: // [reg]
			write_mem16(reg(read_mem(reg(0))), value);
			reg(0) += 1;
			break;
		case 0x02: // imm, ILLEGAL
			break;
		case 0x03: // [imm]
			write_mem16(read_mem16(read_mem16(reg(0))), value);
			reg(0) += 2;
			break;
		case 0x04: // [reg+imm]
			address = reg(read_mem(reg(0)));
			reg(0) += 1;
			address += read_mem16(reg(0));
			reg(0) += 2;
			write_mem16(address, value);
			break;
		case 0x05: // [reg+reg]
			address = reg(read_mem(reg(0)));
			reg(0) += 1;
			address += reg(read_mem(reg(0)));
			reg(0) += 1;
			write_mem16(address, value);
			break;
	}
	printf_debug("    |-oper: %04x\n", value);
	return 0;
}

uint16_t read_operand(uint16_t* value, uint8_t spec){
	printf_debug("    |-spec: %02x\n", spec);
	switch(spec & 0x07){
		case 0x00: // reg
			*value = reg(read_mem(reg(0)));
			reg(0) += 1;
			break;
		case 0x01: // [reg]
			*value = read_mem16(reg(read_mem(reg(0))));
			reg(0) += 1;
			break;
		case 0x02: // imm
			*value = read_mem16(reg(0));
			reg(0) += 2;
			break;
		case 0x03: // [imm]
			*value = read_mem16(read_mem16(reg(0)));
			reg(0) += 2;
			break;
		case 0x04: // [reg+imm]
			*value = reg(read_mem(reg(0)));
			reg(0) += 1;
			*value = read_mem16(*value + read_mem16(reg(0)));
			reg(0) += 2;
			break;
		case 0x05: // [reg+reg]
			*value = reg(read_mem(reg(0)));
			reg(0) += 1;
			*value = read_mem16(*value + reg(read_mem(reg(0))));
			reg(0) += 1;
			break;
	}
	if(spec & 0x08){
		*value &= 0xff;
	}
	printf_debug("    |-oper: %04x\n", *value);
	return 0;
}

uint8_t read_mem(uint32_t addr){
	uint8_t byte = 0;
	if(addr < 0xffff){
		byte = cpu->ram[addr];
	}else if(addr >= 0x10000){
		byte = cpu->vram[addr - 0x10000];
	}
	return byte;
}
uint8_t write_mem(uint32_t addr, uint8_t byte){
	if(addr < 0xffff){
		cpu->ram[addr] = byte;
	}else if(addr >= 0x10000){
		cpu->vram[addr - 0x10000] = byte;
	}
	return byte;
}
uint16_t read_mem16(uint32_t addr){
	uint16_t word = 0;
	if(addr < 0xffff){
		word = (cpu->ram[addr+1] << 8) + cpu->ram[addr];
	}else if(addr >= 0x10000){
		word = (cpu->vram[addr-0x10000+1] << 8) + cpu->vram[addr-0x10000];
	}
	return word;
}
uint16_t write_mem16(uint32_t addr, uint16_t word){
	if(addr < 0xffff){
		cpu->ram[addr+1] = 0xff & (word >> 8);
		cpu->ram[addr] = 0xff & word;
	}else if(addr >= 0x10000){
		cpu->vram[addr - 0x10000] = word;
	}
	return word;
}
