#ifndef CREATE_WINDOW_H
#define CREATE_WINDOW_H

#include<SDL2/SDL.h>
#include<stdint.h>
#include<stdlib.h>

typedef struct Window{
	SDL_Window* win;
	SDL_Renderer* ren;
	SDL_Surface* surf;
	SDL_Texture* tex;
	uint16_t width;
	uint16_t height;
}Window;

char* error_message[256];

int create_window(Window* window, uint16_t w, uint16_t h, const char* title);
int destroy_window(Window* window);
void create_window_error(char* type, const char* error);

#endif
