#ifndef MAIN_H
#define MAIN_H
#include<stdlib.h>
#include<stdint.h>
#include<stdio.h>
#include<stdbool.h>
#include"create_window.h"
#include"cpu.h"

#define NUM_REGS 8

typedef struct CPU{
	bool running;
	uint16_t regs[NUM_REGS];
	uint8_t ram[0x10000];
	uint8_t* vram;
}CPU;

#endif
