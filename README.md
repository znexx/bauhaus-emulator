#Bauhaus-emulator
This is the repository of Bauhaus-emulator, an emulator for the Bauhaus computer architecture. Please see [the wiki](https://bitbucket.org/znexx/bauhaus-emulator/wiki/Home) for more details.
