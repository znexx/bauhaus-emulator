case 0xa8: // out x,y  x=port
case 0xa9: // out x,y  y=data
case 0xab: // out x,y
case 0xac: // out x,y
case 0xad: // out x,y
	read_spec(&spec);
	read_operand(&in1, spec & 0x0f);
	printf_debug("oper1: %04x\n",in1);
	spec >>= 4;
	read_operand(&in2, spec & 0x0f);
	printf_debug("oper2: %04x\n",in2);
	write_port(in1, in2);
	break;
