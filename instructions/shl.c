case 0x40: // shl reg,x,y
case 0x41: // shl [reg],x,y
case 0x43: // shl [imm],x,y
case 0x44: // shl [reg+imm],x,y
case 0x45: // shl [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec >>= 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 << in2);
	break;
