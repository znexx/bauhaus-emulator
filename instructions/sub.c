case 0x38: // sub reg,x,y
case 0x39: // sub [reg],x,y
case 0x3b: // sub [imm],x,y
case 0x3c: // sub [reg+imm],x,y
case 0x3d: // sub [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec >>= 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 << in2);
	break;
