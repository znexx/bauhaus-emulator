case 0x98: // jge reg,x,y
case 0x99: // jge [reg],x,y
case 0x9a: // jge imm,x,y
case 0x9b: // jge [imm],x,y
case 0x9c: // jge [reg+imm],x,y
case 0x9d: // jge [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec = spec >> 8;
	read_operand(&in2, spec);
	read_operand(&out, opcode & 0x07);
	if(in1 >= in2){
		reg(0) = out;
	}else{
		reg(0) += 1;
	}
	break;
