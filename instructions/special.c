case 0x00: // NOP
	break;
case 0xf0: // print regs
	for(out=0; out<NUM_REGS; out++){
		printf("    reg %02x: %04x\n",out,reg(out));
	}
	break;
case 0xf1: // print ram in range
	read_spec(&spec);
	read_operand(&in1, spec);
	spec >>= 8;
	read_operand(&in2, spec);
	for(out = in1; out < in2; out++){
		printf("%04x ",out);
	}
	printf("\n");
	for(out = in1; out < in2; out++){
		printf(" %02x  ",read_mem(out));
	}
	printf("\n");
	break;
case 0xf2: // kill emulator
	cpu->running = false;
	break;
