case 0x20: // xor reg,x,y
case 0x21: // xor [reg],x,y
case 0x23: // xor [imm],x,y
case 0x24: // xor [reg+imm],x,y
case 0x25: // xor [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec >>= 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 << in2);
	break;
