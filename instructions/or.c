case 0x10: // or reg,x,y
case 0x11: // or [reg],x,y
case 0x13: // or [imm],x,y
case 0x14: // or [reg+imm],x,y
case 0x15: // or [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec = spec >> 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 << in2);
	break;
