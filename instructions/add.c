case 0x30: // add reg,x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec = spec >> 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 + in2);
	break;
