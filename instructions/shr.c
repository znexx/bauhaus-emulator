case 0x48: // shr reg,x,y
case 0x49: // shr [reg],x,y
case 0x4b: // shr [imm],x,y
case 0x4c: // shr [reg+imm],x,y
case 0x4d: // shr [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec >>= 8;
	read_operand(&in2, spec);
	write_operand(opcode & 0x07, in1 >> in2);
	break;
