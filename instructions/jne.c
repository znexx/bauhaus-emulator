case 0x88: // jne reg,x,y
case 0x89: // jne [reg],x,y
case 0x8a: // jne imm,x,y
case 0x8b: // jne [imm],x,y
case 0x8c: // jne [reg+imm],x,y
case 0x8d: // jne [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec & 0x0f);
	spec >>= 4;
	read_operand(&in2, spec & 0x0f);
	read_operand(&out, opcode & 0x07);
	if(in1 != in2){
		reg(0) = out;
	}else{
		reg(0) += 1;
	}
	break;
