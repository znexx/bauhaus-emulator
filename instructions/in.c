case 0xa0: // in reg,x  x=port
case 0xa1: // in [reg],x
case 0xa3: // in [imm],x
case 0xa4: // in [reg+imm],x
case 0xa5: // in [reg+reg],x
	read_spec(&spec);
	read_operand(&in1, spec);
	write_operand(opcode & 0x07, read_port(in1));
	break;
