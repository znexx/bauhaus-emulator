case 0x80: // je reg,x,y
case 0x81: // je [reg],x,y
case 0x82: // je imm,x,y
case 0x83: // je [imm],x,y
case 0x84: // je [reg+imm],x,y
case 0x85: // je [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec = spec >> 8;
	read_operand(&in2, spec);
	read_operand(&out, opcode & 0x07);
	if(in1 == in2){
		reg(0) = out;
	}else{
		reg(0) += 1;
	}
	break;
