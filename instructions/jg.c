case 0x90: // jg reg,x,y
case 0x91: // jg [reg],x,y
case 0x92: // jg imm,x,y
case 0x93: // jg [imm],x,y
case 0x94: // jg [reg+imm],x,y
case 0x95: // jg [reg+reg],x,y
	read_spec(&spec);
	read_operand(&in1, spec);
	spec = spec >> 8;
	read_operand(&in2, spec);
	read_operand(&out, opcode & 0x07);
	if(in1 > in2){
		reg(0) = out;
	}else{
		reg(0) += 1;
	}
	break;
